/**************************************
* Author: Mofan Li (login id: mofanl)
* Student number: 741567
* this simple socket server is created for project work for comp30023
*/

#include<stdio.h> 
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>
#include<string.h>
#include<assert.h>

#define MAXMSGSIZE 2000
#define MAXPATHLENGTH 200

void *connect_thread(void *);
char *rootpath;

int main(int argc, char *argv[]){
    int listenfd = 0, connfd = 0, sockaddrsize;
    struct sockaddr_in serv_addr, client_addr;
    rootpath = argv[2];

    listenfd = socket(AF_INET, SOCK_STREAM, 0); //create socket
    memset(&serv_addr, '0', sizeof(serv_addr)); //initialise server address

    serv_addr.sin_family = AF_INET; //Type of address – internet IP
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); //Listen on ANY IP Addr
    serv_addr.sin_port = htons(atoi(argv[1])); // listen to the given port

    bind(listenfd, (struct sockaddr *)&serv_addr , sizeof(serv_addr));
    listen(listenfd, 10);

    sockaddrsize = sizeof(struct sockaddr_in);
    pthread_t thread_id;
    while (1){  // continuely accept requests and create threads to handle it.
        connfd = accept(listenfd, (struct sockaddr*)&client_addr, (socklen_t*)&sockaddrsize);
        
        if (connfd < 0){
            perror("ERROR on accept\n");
            exit(1);
        }

        if( pthread_create( &thread_id, NULL, connect_thread, (void*) &connfd) < 0) { 
            perror("could not create thread"); 
            return 1; 
        } 
    }
    return 0;
}

// this function is the request handler, it deal with the connection to the give socket.
void *connect_thread(void *connfd){
    int sock = *(int*)connfd, c, i;
    char request_msg[MAXMSGSIZE], filepath[MAXPATHLENGTH];
    char response_header[MAXMSGSIZE], msg_buffer[MAXMSGSIZE], *content_type, *relativepath;
    FILE *file;

    recv(sock, request_msg, MAXMSGSIZE, 0);  // receive the request message first.
    
    assert(strcmp(strtok(request_msg, " "), "GET") == 0); // we only deal with GET request now
    relativepath = strtok(NULL, " ");   // extract the path from the request

    strcat(filepath, rootpath);        // concatenate the path
    strcat(filepath, relativepath);

    // identify the mime type.
    if (filepath[strlen(filepath)-2] == 'm'){
        content_type = "Content-Type: text/html\r\n";
    }else if (filepath[strlen(filepath)-2] == 'p'){
        content_type = "Content-Type: image/jpeg\r\n";
    }else if (filepath[strlen(filepath)-2] == 's'){
        content_type = "Content-Type: text/css\r\n";
    }else if (filepath[strlen(filepath)-2] == 'j'){
        content_type = "Content-Type: application/javascript\r\n";
    }else if (filepath[strlen(filepath)-2] == 'x'){
        content_type = "Content-Type: text/plain\r\n";
    }

    // try to locate the file and create response header accordingly
    file = fopen(filepath, "r");
    if (file == NULL){
        strcpy(response_header, "HTTP/1.0 404 Not Found\n");
    }else{
        strcpy(response_header, "HTTP/1.0 200 OK\n");
        strcat(response_header, content_type);
    }    

    // send the header first
    strcat(response_header, "\n");
    send(sock, response_header, strlen(response_header), 0);

    // then read the file into the buffer and send the buffer once it is full.
    if (file != NULL){
        c = fgetc(file);
        i = 0;
        memset(&msg_buffer, 0, sizeof(msg_buffer));
        while (c != EOF){     
            if (i < MAXMSGSIZE){
                msg_buffer[i] = c;
                i++;
                c = fgetc(file);
            }else{
                send(sock, msg_buffer, i, 0);
                memset(msg_buffer, 0, sizeof(msg_buffer));
                i = 0;
            }
        }
        // if reach the end of the file, loop breaks without buffer being full and sent
        // send it manully
        send(sock, msg_buffer, i, 0);
        memset(&msg_buffer, 0, sizeof(msg_buffer));
        fclose(file);
    }

    close(sock); //  close the socket
}